# About

This repository stores a skeleton EDoN SRE Wiki here https://edon-initiative.gitlab.io/containers/edon-sre-wiki/, using a docker container with minoconda, R and Rstudio installed. The image is stored in Packages and regsistries > [Container Registry](https://gitlab.com/edon-initiative/containers/edon-sre-wiki/container_registry) section from within this repository. 

This repository has two aims:

1. A place to build the codebase for the Wiki
2. Be a low-risk sandpit for learning about reproducible compute enviroments, built from containers

# How to use a docker container from this repository

On your local machine run the following to clone the image:

`docker pull registry.gitlab.com/edon-initiative/containers/edon-sre-wiki:latest`

To run the docker container type:

`docker run --rm -p 8787:8787 registry.gitlab.com/edon-initiative/containers/edon-sre-wiki:latest`

Head to `localhost:8787` in your browser and whatever the docker image is confugured to run on port 8787 will appear - currently just as an example Rstudio will be served, but this could be configured to run Jupyter notebooks or something like that.

# Using the container to develop local repositories

You can mount local directories into a docker container and "borrow" resources offered in the docker environment to develop local repositories (as opposed to local python/R installs). To mount a local directory run:

`docker run --rm -p 8787:8787 -v ${PWD}:/mnt registry.gitlab.com/edon-initiative/containers/edon-sre-wiki:latest`

A docker image needs permissions to write to a local filesystem, of which privileges can be specified upfront in the Dockerfile i.e. user 504 for Mac or 1000 for linux. You should have no problems with write access as the default usernames have been included in the Dockerfile - but if you do - please submit an issue.

# VSCode Docker plugin

If you like developing with VSCode - you can install the [Remote Development Extension](https://code.visualstudio.com/docs/remote/remote-overview) that allows you to attach VSCode to a running docker repository. This allows you to attach to containers with local directories mounted, therefore developing as if you were just working entirely from a local machine.

# Installing new tools to the Docker container

The default behavior for Docker images will be to reset after you have shut down the container, so it is recommeneded to re-build the docker image from the Dockerfile in this repository if you want to install new tools to the docker image. But for local testing before this stage you can "commit" changes to the docker image so that they will remain even after shutting down the container. To do this enter the docker shell and install new tools (the container must already be running - so open a new terminal). First find the list of running containers:

`docker ps -a`

and with the image name type (usually you can press tab and it will offer a list to autocomplete):

`docker exec -it <docker-image-name> /bin/bash`

To make any changes permement exit the docker shell with `exit` and then type:

`docker commit registry.gitlab.com/edon-initiative/containers/edon-sre-wiki:<new-tag-name>`

You will then have built a new image that uses the exisitng docker image with a new tag name i.e `registry.gitlab.com/edon-initiative/containers/r_miniconda_container:pycharm_install`

Please note that the new tools that you have installed aren't tracked like in a Dockerfile, so to find out what is installed you would have to run the container an look for them, where in a Dockerfile all tools are specified upfront. Dockerfiles are key for reproducible enviornments so it's always recommended to keep track of experimental changes to the container by continually updating the Dockerfile.
