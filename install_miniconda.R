library(reticulate)
if (!dir.exists("./python-env-test/")) {
  reticulate::conda_create("./python-wiki-env/")
}

py_bin <- grep(getwd(), reticulate::conda_list()$python, value = TRUE)
Sys.setenv(RETICULATE_PYTHON = py_bin)

reticulate::py_install(
  packages = c("vetiver","pins"),
  envname = "./python-wiki-env/",
  pip = TRUE
)
